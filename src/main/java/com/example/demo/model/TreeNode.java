package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;


/**
 * @author YZD
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class TreeNode implements java.io.Serializable {

    /**
     * 当前节点为
     */
    private Long idStr;
    /**
     * 获取 当前节点 ID
     */
    private Long id;

    /**
     * 当前节点标题
     */
    private String title;
    /**
     * 前端框架需要的name字段
     */
    private String name;

    /**
     * 上级节点 ID
     */
    private Long parentId;
    /**
     * 父节点位
     */
    private Long parentIdStr;


    /**
     * 获取 所有子节点
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<TreeNode> children;


    public TreeNode(long id, String title, Long parentId, List<TreeNode> children) {
        this.id = id;
        this.title = title;
        this.parentId = parentId;

        this.children = children;
    }

}
